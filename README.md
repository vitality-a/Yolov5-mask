## 基于Yolov5的口罩检测系统



### 快速开始



1、克隆项目



2、在项目目录下的cmd执行

```cmd
pip install -r requirements.txt
```



3、安装pytorch模块，自行安装，我的版本为

torchaudio-0.10.O+cu113-cp38-cp38-win_amd64.whl

torch-1.10.0+cu113-cp38-cp38-win_amd64.whl

torchvision-0.11.1+cu113-cp38-cp38-win_amd64.whl



4、运行根目录下的detect.py



注：默认是摄像头检测，可以通过修改detect.py中parse_opt()方法中的参数来检测图片或数据

主要参数：
    
    weights = 'runs/train/exp/weights/mask-best.pt'
    source = '0'
    data = ''


